#!/bin/bash
# 
# Licence: Copyleft
# Authors: G.MELLA
# History: 
#    August 2017 : creation for EES2017 School
# Synopsis: 
#    Manage symlinks and or launch javawebstart for a list of jnlp urls
# Install:
# add in your path and run jmmc --install. Then some symlinks will install ins the same directory to launch associated jnlp.

## CONF
JNLPS="http://wsrdata.obs.ujf-grenoble.fr:8080/sphere-server/sphere-client/sphere-client.jnlp
http://www.jmmc.fr/apps/public/AppLauncher/AppLauncher.jnlp
http://www.jmmc.fr/apps/public/Aspro2/Aspro2.jnlp
http://www.jmmc.fr/apps/public/LITpro/LITpro.jnlp
http://www.jmmc.fr/apps/public/SearchCal/SearchCal.jnlp
http://www.jmmc.fr/apps/public/OIFitsExplorer/OIFitsExplorer.jnlp
http://www.jmmc.fr/apps/public/OImaging/OImaging.jnlp
"

## CODE
SCRIPT_PATH=$0
SCRIPT_NAME=$(basename $SCRIPT_PATH)
SCRIPT_ROOT=$(dirname $SCRIPT_NAME)

## BEGIN common functions ##
_abort()
{
    RETVALUE=$1
    shllibEchoInfo "$SCRIPT_NAME exited ${RETVALUE=-with error}"
    exit ${RETVALUE:-1}
}

_exitProperly()
{
    trap true 0 1 2 5 15
    shllibEchoInfo "$SCRIPT_NAME Done"
    exit 
}

# this function must be called in the next following functions to trap signal
# and abort processing
_installTrap()
{
  shllibEchoDebug "Install trap to abort process in case of errors"
  trap _abort 0 1 2 5 15
}

# this function must be called in the next following functions to trap signal
# and abort processing
_uninstallTrap()
{
  shllibEchoDebug "Uninstall trap to abort process properly"
  trap _exit_properly 0 1 2 5 15
}

shllibSetDebugOn()
{
    export ECHOTRACEON=true
}
shllibSetDebugOff()
{
    unset ECHOTRACEON
}
shllibEchoError ()
{
    ARGS=$*
    if [ -n "$ECHOTRACEON" ]
    then
        let idx=0
        for DEBUGTMP in $(caller 0)
        do
            v[$idx]="$DEBUGTMP"
            let idx=$idx+1
        done
        echo -e "ERROR>: ${v[2]}:${v[0]} ${v[1]}\n       $ARGS"  >&2
    else 
        echo -e "ERROR>:       $ARGS"  >&2
    fi
}
shllibEchoInfo ()
{
    ARGS=$*
    echo -e "$ARGS"
}
shllibEchoDebug ()
{
    ARGS=$*
    local DEBUGTMP v
    if [ -n "$ECHOTRACEON" ]
    then
        let idx=0
        for DEBUGTMP in $(caller 0)
        do
            v[$idx]="$DEBUGTMP"
            let idx=$idx+1
        done
        echo -e "DEBUG>: ${v[2]}:${v[0]} ${v[1]}\n       $ARGS" \
        >&2
    fi
}
shllibShowEnv ()
{
  ARGS=$*
  for ARG in $ARGS
  do
    shllibEchoInfo "$ARG=$(eval echo \$$ARG)"
  done
}
## END common functions ##


function _usage(){
  # exit program if one value is given
  let _EXIT_VALUE="$1"
  echo
#  echo "Usage: $SCRIPT_NAME [--list] [--codebase <url>] [--shared_url <url>] [--webroot <dir>] [--help] <ModuleDir>"
  echo "Usage: $SCRIPT_NAME [--help] [--verbose]   [--install|list|runall|remove]"
  echo "  --install        : optional - install sym links of every applications."
  echo "  --remove         : optional - remove  sym link of every applications."
  echo "  --list           : optional - list applications."
  echo "  --runall         : optional - start all javawebstart applications."
#  echo "  --codebase <url> : optional - specify code base attribute of the generated JNLP directory."
#  echo "                     (default value: http://<hostname>/~<username>')"
  echo "  --help           : optional - display this help message."
  echo "  --verbose        : optional - request verbose messages."
  echo "" 
  echo "  This command install and prepare javawebstart launcher commands." 
  echo "    It installs some symlinks near the '$SCRIPT_NAME' command (i.e. under '$SCRIPT_ROOT')."
  echo "  Then you just have to launch them in command line to start the associated javawebstart" 
  echo ""
  if [ -n "$_EXIT_VALUE" ]
  then
    exit $_EXIT_VALUE
  fi

}

function checkEnv(){
  if ! which $SCRIPT_NAME 
  then
    echo "Please add $SCRIPT_ROOT in your PATH:"
    echo "  export PATH=\$PATH:$SCRIPT_ROOT"
  fi
}

function install(){
  for JNLP in $JNLPS
  do
    CMD_NAME=$(basename $JNLP .jnlp)
    CMD_PATH=$SCRIPT_ROOT/$CMD_NAME
    if [ ! -x $CMD_PATH ]
    then
      ln -sv $SCRIPT_PATH $CMD_PATH
    else
      shllibEchoDebug "$CMD_PATH already present"
    fi
  done
}

function remove(){
  for JNLP in $JNLPS
  do
    CMD_NAME=$(basename $JNLP .jnlp)
    CMD_PATH=$SCRIPT_ROOT/$CMD_NAME
    if [ -e $CMD_PATH ]
    then
      rm -v $CMD_PATH
    fi
  done
}

function run(){
 SEARCH_CMD=$1
 for JNLP in $JNLPS
  do
    CMD_NAME=$(basename $JNLP .jnlp)
    if [ "$CMD_NAME" = "$SEARCH_CMD" ]
    then
      javaws $JNLP
    fi
  done
}

function list(){
  shllibEchoInfo "Available commands (in $SCRIPT_ROOT):"
  for JNLP in $JNLPS
  do
    CMD_NAME=$(basename $JNLP .jnlp)
    CMD_PATH=$SCRIPT_ROOT/$CMD_NAME
    if [ ! -x $CMD_PATH ]
    then
      STATUS="installed"
    else
      STATUS="not installed"
    fi
    shllibEchoInfo "  - $CMD_NAME : $JNLP ( symlink command $STATUS )"
  done
}

function runall(){
  shllibEchoInfo "Running all applications:"
  for JNLP in $JNLPS
  do
    CMD_NAME=$(basename $JNLP .jnlp)
    CMD_PATH=$SCRIPT_ROOT/$CMD_NAME
    if [ ! -x $CMD_PATH ]
    then
      STATUS="installed"
    else
      STATUS="not installed"
    fi
    shllibEchoInfo "  - launching $JNLP ( symlink command $STATUS )"
    javaws $JNLP
  done
}


function main(){
  # Analyse options
  #
  # Note that we use `"$@"' to let each command-line parameter expand to a
  # separate word. The quotes around `$@' are essential!
  # We need TEMP as the `eval set --' would nuke the return value of getopt.
  TEMP=`getopt -o "" --long install,list,runall,remove,verbose,help -n "$SCRIPT_NAME" -- "$@"`
  if [ $? != 0 ] ; then _usage 1 >&2 ; exit 1 ; fi
  # Note the quotes around `$TEMP': they are essential!
  eval set -- "$TEMP"
  while true ; do
    case "$1" in
      --list|--runall|--install|--remove ) DO_COMMAND=$1; shift ;;
#      --codebase ) #        CODE_BASE="$2"; shift 2 ;;
      --help ) _usage 0 ;;
      --verbose ) shllibSetDebugOn ; shift ;;
      -- ) shift ; break ;;
      * ) shllibEchoError "Internal error!" ; exit 1 ;;
    esac
  done

  # We could Analyse arguments

  # execute user command ( verbosity may have been adjusted before ) 
  shllibEchoDebug "request to launch command for $DO_COMMAND"
  case $DO_COMMAND in
    --list) list ;;
    --install) install ;;
    --remove) remove ;;
    --runall) runall ;;
    *) checkEnv ;;
  esac
}


case $SCRIPT_NAME in
  jmmc)
    main $*
    ;;
  *)
    run $SCRIPT_NAME
    ;;
esac

