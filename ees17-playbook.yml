---
# role to setup material required by EES17 tutorials
# visit for more details on the school : https://hraetoile2017.sciencesconf.org/
#
# 1. TP sur les perturbations atmosphériques et les techniques de traitement d’image (Frantz)
#    . Images dominées par la diffraction. Limitations & solutions. Introduction aux cours suivants.
# 2. TP sur les données SPHERE (Eric)
# 3. TPs basés sur le MOOC de F. Martinache et les outils du « Jean-Marie Mariotti Center » (JMMC). Réduction et reconstruction d’image avec GRAVITY (Anthony & Florentin)
#
#
# prerequisites :
# run : ansible-galaxy install williamyeh.oracle-java
# copy jdk-8u144-linux-x64.tgz in ./files/
# run : ansible-galaxy install PeterMosmans.virtualbox-guest
 

- hosts: all
  become: yes

  vars:
    # for williamyeh.oracle-java
    java_download_from_oracle: false
    java_install_jce: false
    jdk_tarball_file: jdk-8u144-linux-x64
    java_version: 8
    java_subversion: 144

    # for PeterMosmans.virtualbox-guest
    # auto didn't work on 4.3... because debian's virtual box output returns 4.3.36_Debian (a pull request has been issued)
    # jessie-backport can install 5.1.8_Debianr111374 but additional guest iso does not compile on debian stretch (5.1.26 is fine)
    virtualbox_version: 5.1.26
    virtualbox_x11: true

    # for this playbook
    user_login: ees17
    eso_pipelines_install_dir: /school/eso-pipelines

  gather_facts: true
  tasks:

    - name: "Install owncloud from their repo"
      block:
       - apt_key : url="http://download.opensuse.org/repositories/isv:ownCloud:desktop/Debian_9.0/Release.key" 
       - apt_repository: repo="deb http://download.opensuse.org/repositories/isv:/ownCloud:/desktop/Debian_9.0/ /" filename="owncloud-client" update_cache="yes"
       - apt: name="owncloud-client"

          #    - name: update apt cache
          #apt: update_cache=yes
      when: false

    - name: "Install required system packages"
      apt: name={{ item }}
      with_items:
        - rsync # for coming synchronize tasks
        - xfce4
        - xfce4-xkb-plugin
        - xscreensaver
        - firefox-esr-l10n-fr
        - libx11-dev
        - xemacs21
        - emacs
        - vim-gui-common
        - curl
        - gnuplot
        - libxml2-utils # for litpro libxml2-utils
        - imagemagick
    
    - name: "Create a new partition for school software and data"
      block:
        - parted:
            device: /dev/sda
            number: 3
            part_type: primary
            part_start: 20764672s
            part_end: 104857599s
            state: present
        - filesystem:
            fstype: ext4
            dev: /dev/sda3
        - mount:
            path: /school
            src: /dev/sda3
            fstype: ext4
            state: mounted
    
    - find: 
        paths: "/opt"
        patterns: "*VBoxGuest*"
        recurse: yes
        file_type: directory
      register: vboxguest_paths
    - debug: var=vboxguest_paths
    - include_role:
        name: PeterMosmans.virtualbox-guest
      when: vboxguest_paths.files|length == 0
 

    - include_role:
        name: williamyeh.oracle-java 

    - user:
        name: "{{ user_login}}"
        comment: "{{ user_login | upper}} roscoff tutorial user"
        uid: 1111
        shell: /bin/bash
        group: sudo
        password: $6$DRXV8l4dQ59oXrCN$yodKJoBvN8a4cPwF4kyBTOh92utPlJJzlyUemIsordmMWiPABIVRIgfuiS80ln1yNSC0.0jJrKulacsT01Xsn/

        # CHECK no more usefull because Vagrant file as been configured with ees17
    - lineinfile:
        path: /etc/sudoers
        state: present
        regexp: '^{{user_login}}\s'
        line: '{{ user_login }} ALL=(ALL) NOPASSWD:ALL'

    - name: 'Mira setup '
      block:
        - file:
            path: "{{item}}"
            state: directory
            mode: 0755
            owner: "{{ user_login }}"
          with_items:
            - "/home/ees17/Yorick"
            - "/school/mira_testfiles"
        - blockinfile:
            dest: "/home/ees17/Yorick/custom.i"
            create: yes
            marker: "// {mark} ANSIBLE MANAGED BLOCK"
            block: |
              include, "mira.i"
        - get_url:
            url: "https://raw.githubusercontent.com/emmt/MiRA/master/test/{{item}}"
            dest: "/school/mira_testfiles/{{item}}"
          with_items:
            - mira-demo.i
            - mira-test1.i
            - mira-test2.i

 
    - name: 'Account settings (PATH, screensaver)'
      blockinfile:
        dest: "/home/ees17/.bashrc"
        block: |
          # jmmc script and other installed launchers
          export PATH="$PATH:$HOME/bin"
          # gravity python tools
          export PATH=$PATH:$HOME/python_tools:$HOME/python_tools/gravi_shell:$HOME/python_tools/gravi_quicklook
          export PYTHONPATH=$HOME/Software/sources/python_tools:$PYTHONPATH

          # xaosim
          export PATH=$HOME/.local/bin/:$PATH

          alias yorick="rlwrap yorick"
          # gasgano
          export PATH="$PATH:$HOME/gasgano/bin"
          # esoreflex esorex
          export PATH="$PATH:/school/eso-pipelines/install/bin/"
          echo "Following commands have been install for the school and can be run in the terminal"
          esocmds=$(find /school/eso-pipelines/install/bin/ -name "eso*" | while read line ; do basename $line ; done)
          echo " yorick gasgano fv "$esocmds 
          # let's accept some easy last minute updates, who knows ?
          alias update-EES17VM="curl http://apps.jmmc.fr/ees17/update.sh | bash"
 
    - name: 'Perform custom replacements'
      replace:
        dest: "{{item.dest}}"
        regexp: "{{ item.regexp }}"
        replace: "{{ item.replace }}"
      with_items:
        - {dest: "/etc/lightdm/lightdm.conf",regexp: "#autologin-user=",replace: "autologin-user=ees17",reason: "Auto login"}
        - {dest: "/etc/default/keyboard",regexp: 'XKBLAYOUT="us"',replace: 'XKBLAYOUT="fr,us"',reason: "add fr layout in keyboard"}
 
    - name: "Test if ESO pipelines are present"
      stat: path="{{ eso_pipelines_install_dir }}/install_esoreflex"
      register: eso_file_stat
    - name: "Install some ESO pipelines"
      when: not eso_file_stat.stat.exists
      block:
        - include_role:
            name: eso-pipelines
        - name: "Change ownership to school user of {{ eso_pipelines_install_dir }} "
          file:
            recurse: yes
            state: directory
            path: "{{ eso_pipelines_install_dir }}"
            owner: "{{ user_login }}"
        - name: "Move gasgano to homedir"
          block:
           - stat: path="{{ eso_pipelines_install_dir }}/gasgano-2.4.8"
             register: from_stat
           - stat: path="/home/{{ user_login }}/gasgano"
             register: to_stat
           - command: mv "{{ from_stat.path }}" "/home/{{ user_login }}/gasgano"
             when: from_stat.stat.exists and not to_stat.stat.exists

    - name: "Install system packages required by LITpro"
      apt: name={{ item }} state=present
      with_items:
        - xsltproc
        - xmlstarlet

    - name: alternatives link for "xmlstarlet"
      alternatives:
        name: xml
        link: /usr/bin/xml
        path: "/usr/bin/xmlstarlet"
   
    - name: "Install required science packages"
      apt: name={{ item }} state=present
      with_items:
        #apt: name={{ item }} state=present install_recommends=true
        # increase disk use too much        - science-astronomy
        - tk
        - ftools-fv
        - qfits-tools
        - saods9  
        - python-pip
        - python-rope
        - spyder
        - yorick-mira
        - python-skimage-lib
        # - topcat # no package :(

    - include_role:
        name: jmmc_wisard_ci
    
    - include_role:
        name: jsy1001_bsmem
        
    - include_role:
        name: gravity_python_tools
        
    - include_role:
        name: martinache_xara_xaosim

    - name: "Synchronize some files onto user homedir (LITpro local service, screensaver config...)"
      synchronize:
        src: "files/homedir/"
        dest: "/home/{{user_login}}/"
      become: no
   
    - name: "Synchronize some files onto user /school)"
      synchronize:
        src: "files/schooldir/"
        dest: "/school/"
      become: no
    
    - name: "Install jmmc script to launch jnlps (sphere-client is also included)"
      copy:
        src: files/jmmc
        mode: 0755
        owner: "{{ user_login }}"
        dest: "/home/{{user_login}}/bin/jmmc"

      # CHECK no more usefull because Vagrant file as been configured with ees17 
    - file:
        path: "/home/{{user_login}}"
        state: 'directory'
        owner: "{{ user_login }}"
        recurse: yes

    # TODO run in session './bin/jmmc --runall' before 'vagrant package'

    #    - name: "Reboot"
    #  shell: reboot
