# Recipes to build a virtual box machine using vagrant and ansible for EES17 School

https://hraetoile2017.sciencesconf.org/

## Virtual Machine setup

To start buiding, run:
```bash
vagrant up
```

If you modify the ansible playbook 'ees17-playbook.yml' and want to apply changes, run:
```bash
vagrant provision
```

Virtual machine details are stored into the 'Vagrantfile'. To export the built machine, run:
```bash
vagrant package
```


